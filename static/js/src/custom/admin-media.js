$(function(){
    $('.copy-media-link').click(function() {
        var link = this.dataset.link;
        copyToClipboard(link);
    });
    
});

function copyToClipboard(text) {
    window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
}    
